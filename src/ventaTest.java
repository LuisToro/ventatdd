import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;


class ventaTest {
	
	@Test
	void articuloTest() {
		Producto producto2=new Articulo("bread",3,6);
		Assert.assertEquals(18, producto2.calcularTarifa());
	}
	
	@Test
	void servicioTest() {
		Producto producto1=new Servicio("massage",3);
		Assert.assertEquals(3, producto1.calcularTarifa());
	}
	
	@Test
	void productoTest() {
		List<Producto> venta = new ArrayList<Producto>();
		venta.add(new Articulo("milk",2,2));
		venta.add(new Servicio("manicure",2));
		
		int resultado = 0;
		for(int i=0;i<venta.size();i++) {
			resultado += venta.get(i).calcularTarifa();
		}
		Assert.assertEquals(6, resultado);
	}
}
