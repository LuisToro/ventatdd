
public class Servicio implements Producto {
	private String nombre = "";
	public int precio;
	
	public Servicio(String nombre,int precio) {
        this.nombre = nombre;
        this.precio = precio;
    }
	public boolean estaDisponible(int cantidad) {
		if(cantidad<10)
			return true;
		else
			return false;
	}
	
	public int calcularTarifa() {
		return this.precio;
	}
}
