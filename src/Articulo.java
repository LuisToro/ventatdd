
public class Articulo implements Producto {
	private String nombre = "";
	public int precio;
	public int saldo;
	
	public Articulo(String nombre,int precio,int saldo) {
        this.nombre = nombre;
        this.saldo = saldo;
        this.precio=precio;
    }
	public boolean estaDisponible(int cantidad) {
		if(cantidad<10)
			return true;
		else
			return false;
	}
	
	public int calcularTarifa() {
		return this.saldo*this.precio;
	}
}
